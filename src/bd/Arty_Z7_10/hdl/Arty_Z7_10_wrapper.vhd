--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
--Date        : Fri Mar  8 00:48:04 2019
--Host        : LAPTOP-IP6UM66O running 64-bit major release  (build 9200)
--Command     : generate_target Arty_Z7_10_wrapper.bd
--Design      : Arty_Z7_10_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Arty_Z7_10_wrapper is
  port (
    DDC_In_scl_io : inout STD_LOGIC;
    DDC_In_sda_io : inout STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    HDMI_DDC_scl_io : inout STD_LOGIC;
    HDMI_DDC_sda_io : inout STD_LOGIC;
    HDMI_HPD_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    TMDS_In_clk_n : in STD_LOGIC;
    TMDS_In_clk_p : in STD_LOGIC;
    TMDS_In_data_n : in STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_In_data_p : in STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_clk_n : out STD_LOGIC;
    TMDS_clk_p : out STD_LOGIC;
    TMDS_data_n : out STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_data_p : out STD_LOGIC_VECTOR ( 2 downto 0 );
    shield_IIC_scl_io : inout STD_LOGIC;
    shield_IIC_sda_io : inout STD_LOGIC;
    shield_SPI_io0_io : inout STD_LOGIC;
    shield_SPI_io1_io : inout STD_LOGIC;
    shield_SPI_sck_io : inout STD_LOGIC;
    shield_SPI_ss_io : inout STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
end Arty_Z7_10_wrapper;

architecture STRUCTURE of Arty_Z7_10_wrapper is
  component Arty_Z7_10 is
  port (
    DDC_In_scl_i : in STD_LOGIC;
    DDC_In_scl_o : out STD_LOGIC;
    DDC_In_scl_t : out STD_LOGIC;
    DDC_In_sda_i : in STD_LOGIC;
    DDC_In_sda_o : out STD_LOGIC;
    DDC_In_sda_t : out STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    HDMI_DDC_sda_i : in STD_LOGIC;
    HDMI_DDC_sda_o : out STD_LOGIC;
    HDMI_DDC_sda_t : out STD_LOGIC;
    HDMI_DDC_scl_i : in STD_LOGIC;
    HDMI_DDC_scl_o : out STD_LOGIC;
    HDMI_DDC_scl_t : out STD_LOGIC;
    HDMI_HPD_tri_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    TMDS_clk_p : out STD_LOGIC;
    TMDS_clk_n : out STD_LOGIC;
    TMDS_data_p : out STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_data_n : out STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_In_clk_p : in STD_LOGIC;
    TMDS_In_clk_n : in STD_LOGIC;
    TMDS_In_data_p : in STD_LOGIC_VECTOR ( 2 downto 0 );
    TMDS_In_data_n : in STD_LOGIC_VECTOR ( 2 downto 0 );
    shield_SPI_sck_i : in STD_LOGIC;
    shield_SPI_sck_o : out STD_LOGIC;
    shield_SPI_sck_t : out STD_LOGIC;
    shield_SPI_io0_i : in STD_LOGIC;
    shield_SPI_io0_o : out STD_LOGIC;
    shield_SPI_io0_t : out STD_LOGIC;
    shield_SPI_io1_i : in STD_LOGIC;
    shield_SPI_io1_o : out STD_LOGIC;
    shield_SPI_io1_t : out STD_LOGIC;
    shield_SPI_ss_i : in STD_LOGIC;
    shield_SPI_ss_o : out STD_LOGIC;
    shield_SPI_ss_t : out STD_LOGIC;
    shield_IIC_sda_i : in STD_LOGIC;
    shield_IIC_sda_o : out STD_LOGIC;
    shield_IIC_sda_t : out STD_LOGIC;
    shield_IIC_scl_i : in STD_LOGIC;
    shield_IIC_scl_o : out STD_LOGIC;
    shield_IIC_scl_t : out STD_LOGIC;
    sys_clock : in STD_LOGIC
  );
  end component Arty_Z7_10;
  component IOBUF is
  port (
    I : in STD_LOGIC;
    O : out STD_LOGIC;
    T : in STD_LOGIC;
    IO : inout STD_LOGIC
  );
  end component IOBUF;
  signal DDC_In_scl_i : STD_LOGIC;
  signal DDC_In_scl_o : STD_LOGIC;
  signal DDC_In_scl_t : STD_LOGIC;
  signal DDC_In_sda_i : STD_LOGIC;
  signal DDC_In_sda_o : STD_LOGIC;
  signal DDC_In_sda_t : STD_LOGIC;
  signal HDMI_DDC_scl_i : STD_LOGIC;
  signal HDMI_DDC_scl_o : STD_LOGIC;
  signal HDMI_DDC_scl_t : STD_LOGIC;
  signal HDMI_DDC_sda_i : STD_LOGIC;
  signal HDMI_DDC_sda_o : STD_LOGIC;
  signal HDMI_DDC_sda_t : STD_LOGIC;
  signal shield_IIC_scl_i : STD_LOGIC;
  signal shield_IIC_scl_o : STD_LOGIC;
  signal shield_IIC_scl_t : STD_LOGIC;
  signal shield_IIC_sda_i : STD_LOGIC;
  signal shield_IIC_sda_o : STD_LOGIC;
  signal shield_IIC_sda_t : STD_LOGIC;
  signal shield_SPI_io0_i : STD_LOGIC;
  signal shield_SPI_io0_o : STD_LOGIC;
  signal shield_SPI_io0_t : STD_LOGIC;
  signal shield_SPI_io1_i : STD_LOGIC;
  signal shield_SPI_io1_o : STD_LOGIC;
  signal shield_SPI_io1_t : STD_LOGIC;
  signal shield_SPI_sck_i : STD_LOGIC;
  signal shield_SPI_sck_o : STD_LOGIC;
  signal shield_SPI_sck_t : STD_LOGIC;
  signal shield_SPI_ss_i : STD_LOGIC;
  signal shield_SPI_ss_o : STD_LOGIC;
  signal shield_SPI_ss_t : STD_LOGIC;
begin
Arty_Z7_10_i: component Arty_Z7_10
     port map (
      DDC_In_scl_i => DDC_In_scl_i,
      DDC_In_scl_o => DDC_In_scl_o,
      DDC_In_scl_t => DDC_In_scl_t,
      DDC_In_sda_i => DDC_In_sda_i,
      DDC_In_sda_o => DDC_In_sda_o,
      DDC_In_sda_t => DDC_In_sda_t,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      HDMI_DDC_scl_i => HDMI_DDC_scl_i,
      HDMI_DDC_scl_o => HDMI_DDC_scl_o,
      HDMI_DDC_scl_t => HDMI_DDC_scl_t,
      HDMI_DDC_sda_i => HDMI_DDC_sda_i,
      HDMI_DDC_sda_o => HDMI_DDC_sda_o,
      HDMI_DDC_sda_t => HDMI_DDC_sda_t,
      HDMI_HPD_tri_i(0) => HDMI_HPD_tri_i(0),
      TMDS_In_clk_n => TMDS_In_clk_n,
      TMDS_In_clk_p => TMDS_In_clk_p,
      TMDS_In_data_n(2 downto 0) => TMDS_In_data_n(2 downto 0),
      TMDS_In_data_p(2 downto 0) => TMDS_In_data_p(2 downto 0),
      TMDS_clk_n => TMDS_clk_n,
      TMDS_clk_p => TMDS_clk_p,
      TMDS_data_n(2 downto 0) => TMDS_data_n(2 downto 0),
      TMDS_data_p(2 downto 0) => TMDS_data_p(2 downto 0),
      shield_IIC_scl_i => shield_IIC_scl_i,
      shield_IIC_scl_o => shield_IIC_scl_o,
      shield_IIC_scl_t => shield_IIC_scl_t,
      shield_IIC_sda_i => shield_IIC_sda_i,
      shield_IIC_sda_o => shield_IIC_sda_o,
      shield_IIC_sda_t => shield_IIC_sda_t,
      shield_SPI_io0_i => shield_SPI_io0_i,
      shield_SPI_io0_o => shield_SPI_io0_o,
      shield_SPI_io0_t => shield_SPI_io0_t,
      shield_SPI_io1_i => shield_SPI_io1_i,
      shield_SPI_io1_o => shield_SPI_io1_o,
      shield_SPI_io1_t => shield_SPI_io1_t,
      shield_SPI_sck_i => shield_SPI_sck_i,
      shield_SPI_sck_o => shield_SPI_sck_o,
      shield_SPI_sck_t => shield_SPI_sck_t,
      shield_SPI_ss_i => shield_SPI_ss_i,
      shield_SPI_ss_o => shield_SPI_ss_o,
      shield_SPI_ss_t => shield_SPI_ss_t,
      sys_clock => sys_clock
    );
DDC_In_scl_iobuf: component IOBUF
     port map (
      I => DDC_In_scl_o,
      IO => DDC_In_scl_io,
      O => DDC_In_scl_i,
      T => DDC_In_scl_t
    );
DDC_In_sda_iobuf: component IOBUF
     port map (
      I => DDC_In_sda_o,
      IO => DDC_In_sda_io,
      O => DDC_In_sda_i,
      T => DDC_In_sda_t
    );
HDMI_DDC_scl_iobuf: component IOBUF
     port map (
      I => HDMI_DDC_scl_o,
      IO => HDMI_DDC_scl_io,
      O => HDMI_DDC_scl_i,
      T => HDMI_DDC_scl_t
    );
HDMI_DDC_sda_iobuf: component IOBUF
     port map (
      I => HDMI_DDC_sda_o,
      IO => HDMI_DDC_sda_io,
      O => HDMI_DDC_sda_i,
      T => HDMI_DDC_sda_t
    );
shield_IIC_scl_iobuf: component IOBUF
     port map (
      I => shield_IIC_scl_o,
      IO => shield_IIC_scl_io,
      O => shield_IIC_scl_i,
      T => shield_IIC_scl_t
    );
shield_IIC_sda_iobuf: component IOBUF
     port map (
      I => shield_IIC_sda_o,
      IO => shield_IIC_sda_io,
      O => shield_IIC_sda_i,
      T => shield_IIC_sda_t
    );
shield_SPI_io0_iobuf: component IOBUF
     port map (
      I => shield_SPI_io0_o,
      IO => shield_SPI_io0_io,
      O => shield_SPI_io0_i,
      T => shield_SPI_io0_t
    );
shield_SPI_io1_iobuf: component IOBUF
     port map (
      I => shield_SPI_io1_o,
      IO => shield_SPI_io1_io,
      O => shield_SPI_io1_i,
      T => shield_SPI_io1_t
    );
shield_SPI_sck_iobuf: component IOBUF
     port map (
      I => shield_SPI_sck_o,
      IO => shield_SPI_sck_io,
      O => shield_SPI_sck_i,
      T => shield_SPI_sck_t
    );
shield_SPI_ss_iobuf: component IOBUF
     port map (
      I => shield_SPI_ss_o,
      IO => shield_SPI_ss_io,
      O => shield_SPI_ss_i,
      T => shield_SPI_ss_t
    );
end STRUCTURE;
